package com.example.chessrating.controllers;

import com.example.chessrating.annotation.Monitoring;
import com.example.chessrating.data.PlayersRepository;
import com.example.chessrating.data.RatingDiffRepository;
import com.example.chessrating.model.Player;
import com.example.chessrating.model.RatingDiff;
import com.example.chessrating.util.PlayersInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.min;

@RestController
@RequestMapping("/players")
public class PlayersController {

    private final PlayersRepository playersRepository;
    private final RatingDiffRepository ratingDiffRepository;

    public PlayersController(PlayersRepository playersRepository, RatingDiffRepository ratingDiffRepository) {
        this.playersRepository = playersRepository;
        this.ratingDiffRepository = ratingDiffRepository;
    }

    @Monitoring("ALL_PLAYERS_RECIEVING")
    @GetMapping("")
    public Iterable<Player> getPlayers() {
        return playersRepository.findAll();
    }

    @Monitoring("PLAYER_CREATING")
    @PostMapping("")
    public ResponseEntity<Player> createPlayer(@Valid @RequestBody Player player) {
        playersRepository.save(player);
        return new ResponseEntity<>(player, HttpStatus.CREATED);
    }

    @Monitoring("PLAYER_REMOVING")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> removePlayer (@PathVariable("id") Long id)
    {
        Optional<Player> p = playersRepository.findById(id);
        if (p.isPresent()) {
            playersRepository.delete(p.get());
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Monitoring("PLAYERS_FILLING")
    @GetMapping("/fill")
    public Iterable<Player> fillPlayers() {
        //TODO: should be post
        playersRepository.saveAll(PlayersInitializer.createPlayers());
        return playersRepository.findAll();
    }

    @Monitoring("BEST_PLAYERS_RECIEVING")
    @GetMapping("/bestdiff")
    public List<RatingDiff> getBestDiffPlayers() {
        List<RatingDiff> result = ratingDiffRepository.findBestDiff(LocalDateTime.now().minusMinutes(5));
        //TODO: limit ot setMaxResult in query
        return result.subList(0, min(result.size(), 5));
    }

}
