package com.example.chessrating.util;

import com.example.chessrating.annotation.Monitoring;
import com.example.chessrating.data.PlayersRepository;
import com.example.chessrating.data.RatingDiffRepository;
import com.example.chessrating.model.Player;
import com.example.chessrating.model.RatingDiff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.example.chessrating.util.PlayersInitializer.createPlayers;

@Component
public class GamesScheduler {
    private static final Logger log = LoggerFactory.getLogger(GamesScheduler.class);
    private final PlayersRepository playersRepository;
    private final RatingDiffRepository ratingDiffRepository;


    public GamesScheduler(PlayersRepository playersRepository
            , RatingDiffRepository ratingDiffRepository) {
        this.playersRepository = playersRepository;
        this.ratingDiffRepository = ratingDiffRepository;
        playersRepository.saveAll(createPlayers());
    }

    @Monitoring("GAME_REGISTERING")
    @Scheduled(fixedRate = 30000)
    public void registerGame() {
        //TODO: discover does task run in own thread automatically?
        Player first = randomPlayer(0L);
        Player second = randomPlayer(first.getSchoolId());
        //TODO: null check
        RatingDiff rdFirst = new RatingDiff(first.getId());
        RatingDiff rdSecond = new RatingDiff(second.getId());
        RatingCalculator.calculate(first, second, rdFirst, rdSecond);
        playersRepository.save(first);
        playersRepository.save(second);
        ratingDiffRepository.save(rdFirst);
        ratingDiffRepository.save(rdSecond);

        log.info("Game between {} ({} -> {}) and {} ({} -> {})is registered at {}",
                first.getId(), first.getRating() - rdFirst.getRatingDiff(), first.getRating(),
                second.getId(), second.getRating() - rdSecond.getRatingDiff(), second.getRating(),
                rdSecond.getDateTime());
    }

    private Player randomPlayer(Long schoolId) {
        Page<Player> playerPage;
        if (schoolId == 0L) {
            Long qty = playersRepository.count();
            int idx = (int) (Math.random() * qty);
            playerPage = playersRepository.findAll(PageRequest.of(idx, 1));
        } else {
            Long qty = playersRepository.countBySchoolIdNot(schoolId);
            int idx = (int) (Math.random() * qty);
            playerPage = playersRepository.findBySchoolIdNot(schoolId, PageRequest.of(idx, 1));
        }
        Player p = null;
        if (playerPage.hasContent()) {
            p = playerPage.getContent().get(0);
        }
        return p;
    }
}
