package com.example.chessrating.data;

import com.example.chessrating.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayersRepository extends PagingAndSortingRepository<Player, Long> {

    Page<Player> findBySchoolIdNot(Long schoolId, Pageable pageable);

    Long countBySchoolIdNot(Long schoolId);
}
