package com.example.chessrating.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

@Aspect
@Component
public class MonitoringAspect {

    private final MonitoringRepository monitoringRepository;

    public MonitoringAspect(MonitoringRepository monitoringRepository) {
        this.monitoringRepository = monitoringRepository;
    }

    @Pointcut("@annotation(com.example.chessrating.annotation.Monitoring)")
    public void monitoring() {
    }

    @Around("monitoring()")
    public Object aroundMethod(ProceedingJoinPoint jp) {
        MethodSignature signature = (MethodSignature) jp.getSignature();
        Method method = signature.getMethod();
        String args = Arrays.stream(jp.getArgs()).map(Object::toString).collect(Collectors.joining(","));
        Monitoring annotation = method.getAnnotation(Monitoring.class);

        Date startTime = new Date();
        System.out.println(annotation.value() + " started at " + startTime + " with args "+ args);
        writeEntity(annotation.value()+"_START", 1, args, startTime);
        try {
            return jp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            Date endTime =  new Date();
            System.out.println(annotation.value() + " ended at " + endTime);
            writeEntity(annotation.value()+"_END", 1, args, endTime);
            long duration = endTime.getTime() - startTime.getTime();
            System.out.println(annotation.value() + " took " + duration);
            writeEntity(annotation.value()+"_DURATION", duration, args, endTime);
        }
        return null;
    }

    private void writeEntity(String name, long value, String args, Date timeStamp) {
        MonitoringEntity monitoringEntity = new MonitoringEntity();
        monitoringEntity.setName(name);
        monitoringEntity.setValue(value);
        monitoringEntity.setParameters(args);
        monitoringEntity.setEventTimestamp(timeStamp);
        monitoringRepository.save(monitoringEntity);
    }
}
