# chess-rating

- **get /players** 				список игроков
- **get /players/fill** 			заполняет репозиторий 20ю игроками  (сейчас выполняется при запуске)
- **delete /players/{playerId}** 	удаляет игрока
- **post /players {"name": "{playerName}","schoolId": "{playerSchoolId}"}**		добавляет игрока
- **get /players/bestdiff** 		5 игроков с лучшим изменением рейтинга за 5 минут