package com.example.chessrating;

import com.example.chessrating.data.PlayersRepository;
import com.example.chessrating.model.Player;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.iterableWithSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlayersControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private PlayersRepository playersRepository;

	@Before
	public void setUp() {
		playersRepository.deleteAll();
	}

	@Test
	public void playersControllerShouldReturnNothing() throws Exception {
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(0)));
	}

	@Test
	public void playersControllerShouldReturnPlayers() throws Exception {
		addPlayer();
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.rating", iterableWithSize(1)));
	}

	private void addPlayer() {
		playersRepository.save(createPlayer());
	}

	private Player createPlayer() {
		Player p =  new Player();
		p.setName("TestName");
		p.setRating(1500);
		p.setSchoolId((long) 7);
		return p;
	}

	@Test
	public void playersControllerShouldReturnFilledPlayers() throws Exception {
		mockMvc.perform(get("/players/fill"));
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.schoolId", iterableWithSize(20)));
	}

	@Test
	public void playersControllerCreateNewPlayer() throws Exception {
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(0)));
		mockMvc.perform(post("/players")
				.content(asJsonString(createPlayer()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(1)));
		mockMvc.perform(post("/players")
				.content("{\"name\":\"TestName1\",\"schoolId\":8,\"rating\":1300}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(2)));
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void playersControllerDeletePlayer() throws Exception
	{
		mockMvc.perform(get("/players/fill"));
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(20)));
		Long id = playersRepository.findAll().iterator().next().getId();
		mockMvc.perform(delete("/players/{id}", id) )
				.andExpect(status().isAccepted());
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(19)));
		mockMvc.perform(delete("/players/{id}", id+100) )
				.andExpect(status().isNotFound());
		mockMvc.perform(get("/players"))
				.andExpect(jsonPath("$.*.name", iterableWithSize(19)));
	}

}
