package com.example.chessrating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ChessRatingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChessRatingApplication.class, args);
	}

}
