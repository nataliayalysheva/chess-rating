package com.example.chessrating.util;

import com.example.chessrating.model.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayersInitializer {

    public static Iterable<Player> createPlayers() {
        List<Player> players = new ArrayList<>();
        for (int i = 1; i < 21; i++) {
            Player p = new Player();
            p.setName("Name"+i);
            p.setSchoolId((long) (i % 5 + 1));
            players.add(p);
        }
        return players;
    }
}
