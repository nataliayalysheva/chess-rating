package com.example.chessrating.data;

import com.example.chessrating.model.RatingDiff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RatingDiffRepository extends JpaRepository<RatingDiff, Long> {

    //TODO: tables connections and join to get player name
    @Query("select playerId, sum(ratingDiff) as diff from RatingDiff" +
            " where dateTime > ?1 group by playerId order by diff desc")
    List<RatingDiff> findBestDiff(LocalDateTime dateTime);
}
