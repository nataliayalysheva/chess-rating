package com.example.chessrating.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class RatingDiff {
    @Id
    @GeneratedValue
    private Long id;

    private Long playerId;
    private int ratingDiff;

    private LocalDateTime dateTime = LocalDateTime.now();

    public RatingDiff(Long playerId) {
        this.playerId = playerId;
    }
}
