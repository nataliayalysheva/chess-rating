package com.example.chessrating.annotation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringRepository extends CrudRepository<MonitoringEntity, Long> {

}
