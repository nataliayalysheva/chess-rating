package com.example.chessrating.annotation;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@Entity
public class MonitoringEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private long value;
    private String parameters;
    private Date eventTimestamp;
}