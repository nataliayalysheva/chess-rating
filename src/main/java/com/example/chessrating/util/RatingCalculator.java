package com.example.chessrating.util;

import com.example.chessrating.annotation.Monitoring;
import com.example.chessrating.model.Player;
import com.example.chessrating.model.RatingDiff;
import org.springframework.stereotype.Component;

import static java.lang.Math.pow;

public class RatingCalculator {

    public static void calculate(Player first, Player second
            , RatingDiff ratingDiffFirst, RatingDiff ratingDiffSecond) {
        double result = (int)(Math.random() * 3) / 2.0;
        double expectedFirst = 1.0 / (1 + pow(10, ((double)(second.getRating() - first.getRating())) / 400));

        int firstRatingDiff = (int) Math.round(getKFactor(first) * (result - expectedFirst));
        int secondRatingDiff = (int) Math.round(getKFactor(second) * (expectedFirst - result));

        ratingDiffFirst.setRatingDiff(firstRatingDiff);
        ratingDiffSecond.setRatingDiff(secondRatingDiff);

        first.setRating(first.getRating() + firstRatingDiff);
        second.setRating(second.getRating() + secondRatingDiff);
        //TODO: discover if there is minimum threshold for rating
    }

    private static int getKFactor(Player player) {
        int rating = player.getRating();
        int gamesPlayed = player.getGamesPlayed();

        if (gamesPlayed < 30 || rating < 2300) {
            //assume all school students age is under 18
            return 40;
        } else if (rating < 2400) {
            return 20;
        } else {
            return 10;
        }
    }
}
