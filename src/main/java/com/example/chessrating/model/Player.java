package com.example.chessrating.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Player {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    //
    private Long schoolId;
    private int rating = 1000;
    private int gamesPlayed = 0;
}
